package com.getjavajob.myasnykh;

import com.getjavajob.myasnykh.dto.FileInfo;
import com.getjavajob.myasnykh.exceptions.CannotSaveFileException;
import com.getjavajob.myasnykh.exceptions.FileWithSuchNameAlreadyExistsException;
import com.getjavajob.myasnykh.exceptions.NoFileWithSuchIdExistsException;
import com.getjavajob.myasnykh.service.StorageService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = "classpath:web-test-context.xml")
public class StorageControllerIntegrationTest {

    @Autowired
    private WebApplicationContext webAppContext;

    @Autowired
    private StorageService storageService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
    }

    @After
    public void tearDown() {
        reset(storageService);
    }

    @Test
    public void getIndexPage_forwards_to_searchPage() throws Exception {
        this.mockMvc.perform(
                get("/index")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("searchFile"))
                .andReturn();
    }

    @Test
    public void getUploadPage_forwards_to_uploadFilePage() throws Exception {
        this.mockMvc.perform(
                get("/upload")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("uploadFile"))
                .andReturn();
    }

    @Test
    public void postFile_returnsFileId() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "testFile.txt", "multipart/form-data", new byte[]{1, 2, 3, 4});
        when(storageService.saveFile(eq("testFile.txt"), any(InputStream.class))).thenReturn("1234");
        this.mockMvc.perform(
                fileUpload("/upload")
                        .file(file)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("{\"fileId\":\"1234\"}"))
                .andReturn();
    }

    @Test
    public void postFile_returnsError_FileWithSuchNameAlreadyExists() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "testFile.txt", "multipart/form-data", new byte[]{1, 2, 3, 4});
        when(storageService.saveFile(eq("testFile.txt"), any(InputStream.class))).thenThrow(new FileWithSuchNameAlreadyExistsException());
        this.mockMvc.perform(
                fileUpload("/upload")
                        .file(file)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("{\"error\":\"File with such name already exists in server. Rename your file.\"}"))
                .andReturn();
    }

    @Test
    public void postFile_returnsError_FailedToSaveFile() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "testFile.txt", "multipart/form-data", new byte[]{1, 2, 3, 4});
        when(storageService.saveFile(eq("testFile.txt"), any(InputStream.class))).thenThrow(new CannotSaveFileException());
        this.mockMvc.perform(
                fileUpload("/upload")
                        .file(file)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("{\"error\":\"Failed to save file.\"}"))
                .andReturn();
    }

    @Test
    public void postFile_returnsError_SomethingWentWrong() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "testFile.txt", "multipart/form-data", new byte[]{});
        this.mockMvc.perform(
                fileUpload("/upload")
                        .file(file)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("{\"error\":\"You failed to upload testFile.txt because the file was empty.\"}"))
                .andReturn();
    }

    @Test
    public void getSearchPage_forwards_to_searchPage() throws Exception {
        this.mockMvc.perform(
                get("/search")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("searchFile"))
                .andReturn();
    }

    @Test
    public void getFile_returnsFile() throws Exception {
        when(storageService.getFileById("1")).thenReturn(new ByteArrayInputStream(new byte[]{1, 2, 3}));
        this.mockMvc.perform(
                get("/download")
                        .param("id", "1")
                        .accept(MediaType.TEXT_HTML_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().bytes(new byte[]{1, 2, 3}))
                .andReturn();
    }

    @Test
    public void getFile() throws Exception {
        when(storageService.getFileById("1")).thenThrow(new NoFileWithSuchIdExistsException());
        this.mockMvc.perform(
                get("/download")
                        .param("id", "1")
                        .accept(MediaType.TEXT_HTML_VALUE))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void searchFiles_returns_FileList() throws Exception {
        //there is list of FileInfo
        List<FileInfo> list = new ArrayList<>();
        FileInfo first = new FileInfo("1", "test1");
        FileInfo second = new FileInfo("2", "test2");
        list.add(first);
        list.add(second);

        when(storageService.searchFileByName("test")).thenReturn(list);
        this.mockMvc.perform(
                get("/search")
                        .param("name", "test")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is("1")))
                .andExpect(jsonPath("$[0].name", is("test1")))
                .andExpect(jsonPath("$[1].id", is("2")))
                .andExpect(jsonPath("$[1].name", is("test2")))
                .andReturn();
    }

    @Test
    public void remove_returns_true_if_file_was_removed() throws Exception {
        when(storageService.removeFileById("1")).thenReturn(true);
        this.mockMvc.perform(
                get("/remove")
                        .param("id", "1")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string("true"))
                .andReturn();
    }

    @Test
    public void remove_returns_false_if_file_wasNot_removed() throws Exception {
        when(storageService.removeFileById("1")).thenThrow(new NoFileWithSuchIdExistsException());
        this.mockMvc.perform(
                get("/remove")
                        .param("id", "1")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string("false"))
                .andReturn();
    }
}
