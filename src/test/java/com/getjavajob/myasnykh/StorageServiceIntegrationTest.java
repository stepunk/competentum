package com.getjavajob.myasnykh;

import com.getjavajob.myasnykh.dto.FileInfo;
import com.getjavajob.myasnykh.exceptions.FileWithSuchNameAlreadyExistsException;
import com.getjavajob.myasnykh.index.Index;
import com.getjavajob.myasnykh.repository.Repository;
import com.getjavajob.myasnykh.service.StorageService;
import com.getjavajob.myasnykh.service.StorageServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StorageServiceIntegrationTest {
    StorageService storageService;
    private Repository repository;
    private Index index;

    @Before
    public void setup() {
        repository = mock(Repository.class);
        index = mock(Index.class);
        this.storageService = new StorageServiceImpl(repository, index);
    }

    @Test
    public void saveFile_returns_Id() {
        when(index.fileNameExists("testFile.txt")).thenReturn(false);
        when(repository.saveFile(eq("testFile.txt"), any(InputStream.class))).thenReturn("1234");
        when(index.fileIdExists("1234")).thenReturn(false);
        String id = storageService.saveFile("testFile.txt", new ByteArrayInputStream(new byte[]{1, 2, 3, 4}));

        assertNotNull(id);
        assertEquals("1234", id);
    }

    @Test
    public void saveFile_throws_FileWithSuchNameAlreadyExistsException() {
        when(index.fileNameExists("testFile.txt")).thenReturn(true);

        try {
            storageService.saveFile("testFile.txt", new ByteArrayInputStream(new byte[]{1, 2, 3, 4}));
            fail("Expected an FileWithSuchNameAlreadyExistsException to be thrown");
        } catch (FileWithSuchNameAlreadyExistsException e) {
            assertEquals(e.getMessage(), "File with such name already exists");
        }
    }

    @Test
    public void searchFileByName_returns_List_FileInfo() {
        //there is expectedList of FileInfo
        List<FileInfo> expectedList = new ArrayList<>();
        FileInfo first = new FileInfo("1", "test1");
        FileInfo second = new FileInfo("2", "test2");
        expectedList.add(first);
        expectedList.add(second);
        when(index.getFiles("test")).thenReturn(expectedList);

        List<FileInfo> actualList = storageService.searchFileByName("test");
        assertNotNull(actualList);
        assertFalse(actualList.isEmpty());
        assertEquals(expectedList, actualList);
    }

    @Test
    public void getFileById_returns_InputStream() throws IOException {
        //there is FileInfo
        FileInfo fileInfo = new FileInfo("1", "test");
        ByteArrayInputStream expectedInputStream = new ByteArrayInputStream(new byte[]{1, 2, 3, 4});
        when(index.getFileById("1")).thenReturn(fileInfo);
        when(repository.getFile("test")).thenReturn(expectedInputStream);

        InputStream actualInputStream = storageService.getFileById("1");
        assertNotNull(actualInputStream);
        assertEquals(expectedInputStream, actualInputStream);
    }

    @Test
    public void getFileNameById_returns_FileName() throws IOException {
        //there is FileInfo
        FileInfo fileInfo = new FileInfo("1", "test");
        when(index.getFileById("1")).thenReturn(fileInfo);

        String actualFileName = storageService.getFileNameById("1");
        assertNotNull(actualFileName);
        assertEquals(fileInfo.getName(), actualFileName);
    }

    @Test
    public void removeFileById_returns_true_if_file_was_removed() throws IOException {
        //there is FileInfo
        FileInfo fileInfo = new FileInfo("1", "test");
        when(index.getFileById("1")).thenReturn(fileInfo);
        when(repository.removeFile("test")).thenReturn(true);

        boolean actual = storageService.removeFileById("1");
        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    public void removeFileById_returns_false_if_file_wasNot_removed() throws IOException {
        //there is FileInfo
        FileInfo fileInfo = new FileInfo("1", "test");
        when(index.getFileById("1")).thenReturn(fileInfo);
        when(repository.removeFile("test")).thenReturn(false);

        boolean actual = storageService.removeFileById("1");
        boolean expected = false;
        assertEquals(expected, actual);
    }
}
