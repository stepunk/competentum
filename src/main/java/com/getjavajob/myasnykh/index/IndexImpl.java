package com.getjavajob.myasnykh.index;


import com.getjavajob.myasnykh.dto.FileInfo;
import com.getjavajob.myasnykh.exceptions.NoFileWithSuchIdExistsException;
import com.getjavajob.myasnykh.idcalculator.IdCalculator;
import com.getjavajob.myasnykh.repository.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IndexImpl implements Index {
    private static final Logger logger = LoggerFactory.getLogger(IndexImpl.class);
    private final Map<String, List<FileInfo>> files;
    private final Repository repository;

    public IndexImpl(Repository repository) throws NoSuchAlgorithmException {
        files = new HashMap<>();
        this.repository = repository;
        indexStorage();
    }

    private void indexStorage() {
        List<File> allFilesInDirectory = repository.getAllFilesInDirectory();
        if (allFilesInDirectory != null) {
            for (File file : allFilesInDirectory) {
                IdCalculator idCalculator = new IdCalculator();
                byte[] buffer = new byte[2 * 1024 * 1024];
                try (FileInputStream input = new FileInputStream(file)) {
                    while (input.read(buffer) != -1) {
                        idCalculator.update(buffer);
                    }
                } catch (FileNotFoundException e) {
                    logger.error("By setting indexes for all files in directory, file from files list was not found. Cause", e);
                    continue;
                } catch (IOException e) {
                    logger.error("By setting indexes for all files in directory something went wrong. Cause ", e);
                    continue;
                }
                String id = idCalculator.getFileId();
                FileInfo fileInfo = new FileInfo(id, file.getName());
                addFile(fileInfo);
            }
        }
    }

    public boolean addFile(FileInfo file) {
        String id = file.getId();
        synchronized (files) {
            List<FileInfo> infoFiles = files.get(id);
            if (infoFiles == null) {
                infoFiles = new ArrayList<>();
                files.put(id, infoFiles);
            }
            infoFiles.add(file);
        }
        return true;
    }

    public FileInfo getFileById(String id) {
        synchronized (files) {
            List<FileInfo> infoFiles = files.get(id);
            if (infoFiles == null || infoFiles.isEmpty()) {
                logger.info("Attempt to get file with non existing ID=" + id);
                throw new NoFileWithSuchIdExistsException("There is not such file. Nonexistent id= " + id);
            }
            return infoFiles.get(0);
        }
    }

    public void removeFilesById(String id) {
        synchronized (files) {
            files.remove(id);
        }
    }

    public List<FileInfo> getFiles(String name) {
        List<FileInfo> result = new ArrayList<>();
        synchronized (files) {
            for (List<FileInfo> list : files.values()) {
                for (FileInfo file : list) {
                    if (file.getName().contains(name)) {
                        result.add(file);
                    }
                }
            }
        }
        return result;
    }

    public boolean fileIdExists(String id) {
        synchronized (files) {
            if (!files.isEmpty()) {
                return files.get(id) != null && !files.isEmpty();
            }
        }
        return false;
    }

    public boolean fileNameExists(String name) {
        synchronized (files) {
            for (List<FileInfo> list : files.values()) {
                for (FileInfo file : list) {
                    if (file.getName().equals(name)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}