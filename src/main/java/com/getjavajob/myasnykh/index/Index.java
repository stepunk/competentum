package com.getjavajob.myasnykh.index;


import com.getjavajob.myasnykh.dto.FileInfo;

import java.util.List;

public interface Index {

    boolean addFile(FileInfo file);

    FileInfo getFileById(String id);

    void removeFilesById(String id);

    List<FileInfo> getFiles(String name);

    boolean fileIdExists(String id);

    boolean fileNameExists(String name);
}
