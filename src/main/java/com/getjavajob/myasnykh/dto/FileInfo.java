package com.getjavajob.myasnykh.dto;

public class FileInfo {
    private String id;
    private String name;

    public FileInfo(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileInfo file = (FileInfo) o;

        if (id != null ? !id.equals(file.id) : file.id != null) return false;
        return !(name != null ? !name.equals(file.name) : file.name != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
