package com.getjavajob.myasnykh.repository;

import com.getjavajob.myasnykh.exceptions.CannotSaveFileException;
import com.getjavajob.myasnykh.exceptions.CannotWriteInDirectoryException;
import com.getjavajob.myasnykh.exceptions.FileWithSuchNameAlreadyExistsException;
import com.getjavajob.myasnykh.exceptions.NoSuchDirectoryException;
import com.getjavajob.myasnykh.idcalculator.IdCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class RepositoryImpl implements Repository {
    private static final Logger logger = LoggerFactory.getLogger(RepositoryImpl.class);
    private File directory;

    public RepositoryImpl(String path) {
        File directory = new File(path);
        if (!directory.isDirectory()) {
            logger.error("Directory: " + path + " does not exist.Trying to set a non-existent directory");
            throw new NoSuchDirectoryException("There is no directory: " + path);
        }
        if (!directory.canWrite()) {
            logger.error("it is impossible to write in directory:" + path);
            throw new CannotWriteInDirectoryException("Can not write in directory:" + path);
        }
        this.directory = directory;
    }

    @Override
    public List<File> getAllFilesInDirectory() {
        if (directory.listFiles() == null) {
            return null;
        }
        return Arrays.asList(directory.listFiles());
    }

    @Override
    public String saveFile(String name, InputStream inputStream) {
        File file = new File(directory.getAbsolutePath() + "/" + name);
        if (file.exists()) {
            throw new FileWithSuchNameAlreadyExistsException("File already exist. File name:" + name);
        }
        try (FileOutputStream output = new FileOutputStream(file); InputStream input = inputStream) {
            IdCalculator idCalculator = new IdCalculator();
            byte[] buffer = new byte[2 * 1024 * 1024];
            int bytesRead;
            while ((bytesRead = input.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
                idCalculator.update(buffer);
            }
            return idCalculator.getFileId();
        } catch (IOException e) {
            logger.error("File " + name + "was not saved. Cause:", e);
            throw new CannotSaveFileException(e);
        }
    }

    @Override
    public boolean removeFile(String name) {
        File file = new File(directory.getAbsolutePath() + "/" + name);
        return file.delete();
    }

    @Override
    public InputStream getFile(String name) throws IOException {
        File file = new File(directory.getAbsolutePath() + "/" + name);
        if (!file.exists()) {
            logger.error("File not found.File:" + name);
            throw new IOException("File not found.File:" + name);
        }
        if (!file.canRead()) {
            logger.error("Can not read file:" + name);
            throw new IOException("Can not read file:" + name);
        }
        return new FileInputStream(file);
    }
}
