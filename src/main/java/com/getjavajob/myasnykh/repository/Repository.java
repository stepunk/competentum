package com.getjavajob.myasnykh.repository;

import com.getjavajob.myasnykh.exceptions.CannotSaveFileException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface Repository {

    List<File> getAllFilesInDirectory();

    String saveFile(String name, InputStream inputStream) throws CannotSaveFileException;

    boolean removeFile(String name);

    InputStream getFile(String name) throws IOException;
}
