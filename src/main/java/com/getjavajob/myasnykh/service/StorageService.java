package com.getjavajob.myasnykh.service;


import com.getjavajob.myasnykh.dto.FileInfo;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public interface StorageService {
    String saveFile(String fileName, InputStream inputStream);

    InputStream getFileById(String fileId) throws IOException;

    String getFileNameById(String fileId);

    boolean removeFileById(String fileId);

    List<FileInfo> searchFileByName(String name);
}
