package com.getjavajob.myasnykh.service;

import com.getjavajob.myasnykh.dto.FileInfo;
import com.getjavajob.myasnykh.exceptions.FileWithSuchNameAlreadyExistsException;
import com.getjavajob.myasnykh.index.Index;
import com.getjavajob.myasnykh.repository.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class StorageServiceImpl implements StorageService {
    private Repository repository;

    private Index index;

    public StorageServiceImpl(Repository repository, Index index) {
        this.repository = repository;
        this.index = index;
    }

    @Override
    public String saveFile(String fileName, InputStream inputStream) {
        //check if file with the same name already exists
        if (index.fileNameExists(fileName)) {
            throw new FileWithSuchNameAlreadyExistsException("File with such name already exists");
        }

        String id = repository.saveFile(fileName, inputStream);

        //check if file with the same content already exists
        if (index.fileIdExists(id)) {
            repository.removeFile(fileName);
        }

        FileInfo newFile = new FileInfo(id, fileName);
        index.addFile(newFile);
        return id;
    }

    @Override
    public List<FileInfo> searchFileByName(String name) {
        return index.getFiles(name);
    }

    @Override
    public InputStream getFileById(String fileId) throws IOException {
        FileInfo file = index.getFileById(fileId);
        return repository.getFile(file.getName());
    }

    @Override
    public String getFileNameById(String fileId) {
        FileInfo file = index.getFileById(fileId);
        return file.getName();
    }

    @Override
    public boolean removeFileById(String fileId) {
        FileInfo file = index.getFileById(fileId);
        if (repository.removeFile(file.getName())) {
            index.removeFilesById(fileId);
            return true;
        }
        return false;
    }
}
