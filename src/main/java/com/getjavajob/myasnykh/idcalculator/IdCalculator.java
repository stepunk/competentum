package com.getjavajob.myasnykh.idcalculator;

import com.getjavajob.myasnykh.exceptions.CannotGetIdException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class IdCalculator {
    private final MessageDigest md;

    public IdCalculator() {
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new CannotGetIdException(e);
        }
    }

    public void update(byte[] input) {
        md.update(input);
    }

    public String getFileId() {
        byte[] digest = md.digest();
        return String.format("%064x", new BigInteger(1, digest));
    }
}