package com.getjavajob.myasnykh;


import com.getjavajob.myasnykh.exceptions.NoSuchDirectoryException;
import com.getjavajob.myasnykh.index.Index;
import com.getjavajob.myasnykh.index.IndexImpl;
import com.getjavajob.myasnykh.repository.Repository;
import com.getjavajob.myasnykh.repository.RepositoryImpl;
import com.getjavajob.myasnykh.service.StorageService;
import com.getjavajob.myasnykh.service.StorageServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.security.NoSuchAlgorithmException;

@EnableWebMvc
@ComponentScan(basePackages = {"com.getjavajob.myasnykh.ui.controllers"})
@Configuration
public class AppConfig extends WebMvcConfigurerAdapter {

    @Value("${directory.path:C:/dev/server}")
    private String path;

    @Bean
    public static PropertyPlaceholderConfigurer properties() {
        return new PropertyPlaceholderConfigurer();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

    @Bean
    public Repository getRepository() throws NoSuchDirectoryException {
        return new RepositoryImpl(path);
    }

    @Bean
    public Index getIndex() throws NoSuchDirectoryException, NoSuchAlgorithmException {
        return new IndexImpl(getRepository());
    }

    @Bean
    public StorageService getService() throws NoSuchAlgorithmException {
        return new StorageServiceImpl(getRepository(), getIndex());
    }

    @Bean
    public InternalResourceViewResolver getInternalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver createMultipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setDefaultEncoding("utf-8");
        return resolver;
    }
}