package com.getjavajob.myasnykh.ui.controllers;


import com.getjavajob.myasnykh.dto.FileInfo;
import com.getjavajob.myasnykh.exceptions.CannotSaveFileException;
import com.getjavajob.myasnykh.exceptions.FileWithSuchNameAlreadyExistsException;
import com.getjavajob.myasnykh.exceptions.NoFileWithSuchIdExistsException;
import com.getjavajob.myasnykh.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class StorageController {
    private static final Logger logger = LoggerFactory.getLogger(StorageController.class);

    @Autowired
    private StorageService service;

    @RequestMapping(path = {"/index", "/"}, method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView getIndexPage() {
        return new ModelAndView("searchFile");
    }

    @RequestMapping(path = "upload", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView uploadFilePage() {
        return new ModelAndView("uploadFile");
    }

    @ResponseBody
    @RequestMapping(path = "upload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> uploadFile(@RequestParam("file") MultipartFile file) {
        Map<String, String> model = new HashMap<>();
        String name = file.getOriginalFilename();

        if (!file.isEmpty()) {
            try {
                String fileId = service.saveFile(name, file.getInputStream());
                model.put("fileId", fileId);
            } catch (FileWithSuchNameAlreadyExistsException e) {
                logger.error("User tried to upload file with existing name", e);
                model.put("error", "File with such name already exists in server. Rename your file.");
            } catch (CannotSaveFileException e) {
                logger.error("Impossible to save the file", e);
                model.put("error", "Failed to save file.");
            } catch (IOException e) {
                logger.error("Failed to get bytes from uploaded file");
                model.put("error", "Something went wrong.");
            }
        } else {
            logger.error("User tried to upload empty file");
            model.put("error", "You failed to upload " + name + " because the file was empty.");
        }
        return model;
    }

    @RequestMapping(path = "download", method = RequestMethod.GET)
    public void download(@RequestParam("id") String fileId, HttpServletResponse response) {
        try (OutputStream outStream = response.getOutputStream(); InputStream input = service.getFileById(fileId)) {
            byte[] buffer = new byte[2 * 1024 * 1024];
            int bytesRead;
            while ((bytesRead = input.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            String fileName = service.getFileNameById(fileId);
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
        } catch (NoFileWithSuchIdExistsException e) {
            logger.error("User tried to download nonexistent file. Cause:", e);
        } catch (IOException e) {
            logger.error("Failed to download file. Cause:", e);
        }
    }


    @RequestMapping(path = "search", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String getSearchPage() {
        return "searchFile";
    }

    @ResponseBody
    @RequestMapping(path = "search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FileInfo> search(@RequestParam("name") String fileName) {
        return service.searchFileByName(fileName);
    }


    @ResponseBody
    @RequestMapping(path = "remove", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean remove(@RequestParam("id") String fileId) {
        try {
            return service.removeFileById(fileId);
        } catch (NoFileWithSuchIdExistsException e) {
            logger.error("User tried to remove file with nonexistent id");
            return false;
        }
    }
}
