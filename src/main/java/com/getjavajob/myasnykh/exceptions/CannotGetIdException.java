package com.getjavajob.myasnykh.exceptions;

public class CannotGetIdException extends RuntimeException {
    public CannotGetIdException() {
    }

    public CannotGetIdException(String message) {
        super(message);
    }

    public CannotGetIdException(Throwable cause) {
        super(cause);
    }
}
