package com.getjavajob.myasnykh.exceptions;

public class FileWithSuchNameAlreadyExistsException extends RuntimeException {
    public FileWithSuchNameAlreadyExistsException() {
    }

    public FileWithSuchNameAlreadyExistsException(String message) {
        super(message);
    }
}
