package com.getjavajob.myasnykh.exceptions;

public class NoSuchDirectoryException extends RuntimeException {
    public NoSuchDirectoryException() {
    }

    public NoSuchDirectoryException(String message) {
        super(message);
    }
}
