package com.getjavajob.myasnykh.exceptions;

public class NoFileWithSuchIdExistsException extends RuntimeException {
    public NoFileWithSuchIdExistsException() {
    }

    public NoFileWithSuchIdExistsException(String message) {
        super(message);
    }
}
