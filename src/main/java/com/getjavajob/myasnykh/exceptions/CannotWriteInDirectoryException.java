package com.getjavajob.myasnykh.exceptions;

/**
 * Created by User on 01.12.2015.
 */
public class CannotWriteInDirectoryException extends RuntimeException {
    public CannotWriteInDirectoryException() {
    }

    public CannotWriteInDirectoryException(String message) {
        super(message);
    }

    public CannotWriteInDirectoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotWriteInDirectoryException(Throwable cause) {
        super(cause);
    }

    public CannotWriteInDirectoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
