<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="UTF-8">
    <title>Competentum</title>
    <link href="<c:url value="/static/css/bootstrap.css"/>" rel="stylesheet">
    <script type="text/javascript" src="<c:url value="/static/js/jquery-2.1.4.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/static/js/bootstrap.js"/>"></script>
</head>