<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="head.jsp"></jsp:include>
<body>
<div class="container">
    <div class="container-fluid">
        <jsp:include page="header.jsp"></jsp:include>
        <div class="container-fluid">

            <div class="row">
                <h3 class="text-center">Upload File</h3>
            </div>
            <div class="row" id="show">
            </div>

            <div class="row" style="margin-top: 10px">
                <form method="POST" action="uploadFile" enctype="multipart/form-data">
                    File to upload: <input type="file" name="file" id="file"><br/>

                </form>
                <input type="submit" value="Upload" id="uploadFile">
                Press here to upload the file!
            </div>

        </div>
    </div>
</div>

<script type="text/javascript" src="<c:url value="/static/js/upload.js"/>"></script>
</body>
</html>