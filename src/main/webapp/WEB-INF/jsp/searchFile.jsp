<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="head.jsp"></jsp:include>
<body>
<div class="container">
    <div class="container-fluid">
        <jsp:include page="header.jsp"></jsp:include>
        <div class="container-fluid">
            <div class="row">
                <h3 class="text-center">Search file</h3>
            </div>

            <div class="row" id="showError">
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <input type="text" id="name" name="name" class="form-control"
                           placeholder="Input file name">
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-success" id="searchButton">Search
                    </button>
                </div>
            </div>

            <hr>

            <div class="row">
                <h2 class="text-center">Files</h2>
            </div>

            <div class="row">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="col-md-4 text-center">Name</th>
                        <th class="col-md-6 text-center">ID</th>
                        <th class="col-md-1 text-center" style="border: none"></th>
                        <th class="col-md-1 text-center" style="border: none"></th>

                    </tr>
                    </thead>
                    <tbody id="filesTable">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<c:url value="/static/js/search.js"/>"></script>
</body>
</html>