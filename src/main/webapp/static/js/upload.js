$("#uploadFile").click(function () {
    var formData = new FormData();
    var file = document.getElementById("file").files[0];
    formData.append("file", file);


    $.ajax({
        url: 'upload',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            document.getElementById('show').innerHTML = '';
            if (data['error'] != null) {
                $("#show").append("" +
                    "<h2 class='text-center text-danger'>Error</h2>" +
                    "<h3 class='text-center text-danger'>" + data['error'] + "</h3>");
            } else {
                $("#show").append("" +
                    "<h3 class='text-center text-success'>File was successfully uploaded</h3>" +
                    "<h4 class='text-center text-success'>File id= " + data['fileId'] + "</h3>");
            }
        }
    });
});
