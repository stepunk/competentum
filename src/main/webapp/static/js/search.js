function removeFile(x) {
    $.ajax({
        type: "GET",
        url: "remove",
        data: {id: x.value},
        success: function (data) {
            if (data) {
                $("." + x.value).remove();
            }
        }
    });

}

function downloadFile(id) {
    window.location = "download?id=" + id;
}

$("#searchButton").click(function () {
    var name = $("#name").val();
    document.getElementById('filesTable').innerHTML = '';
    $.ajax({
        type: "GET",
        url: "search",
        data: {name: name},
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                $("#filesTable").append("" +
                    "<tr class='" + data[i].id + "'>" +
                    "<td class='text-center'>" + data[i].name + "</td>" +
                    "<td class='text-center'>" + data[i].id + "</td>" +
                    "<td style='border: none'>" +
                    "<button type='submit' class='btn btn-primary' onclick='downloadFile(this.value)' value='" + data[i].id + "'>download" +
                    "</button></td>" +
                    "<td style='border: none'>" +
                    "<button type='submit' class='btn btn-danger' onclick='removeFile(this)' value='" + data[i].id + "'>remove" +
                    "</button></td>" +
                    "</tr>"
                );
            }
        }
    });
});
